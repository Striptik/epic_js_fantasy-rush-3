/*
** get_next_line.h for get_next_line in /home/moreau_0/epic_js_fantasy-rush-3
** 
** Made by 
** Login   <moreau_0@epitech.net>
** 
** Started on  Sat May 10 04:26:08 2014 
** Last update Sat May 10 04:26:09 2014 
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_
# define GET_BUFF 1024

# include <unistd.h>
# include <stdlib.h>

char	*get_next_line(const int fd);
char	*my_end_of(char *buff);
char	*this_is_the_end(char *total, char **left, int ret);
int	my_strchar(char *str, char c);

#endif /* !GET_NEXT_LINE_H_ */
