/*
** header.c for header in /home/loisel_k/Taff/C/C-Prog-Elem/B2/epic_js_fantasy-rush-3/epic_editor
** 
** Made by loisel_k
** Login   <loisel_k@epitech.net>
** 
** Started on  Sun May 11 17:53:28 2014 loisel_k
** Last update Sat May 10 08:46:35 2014 Augustin Saint Olive
*/

#include <stdio.h>
#include <string.h>
#include "get_next_line.h"
#include "editor.h"

int			init_header(FILE *file)
{
  int			nb_room;

  printf("Welcome in Edit Game mode.\n\n");
  printf("Writing Magic number.\n");
  fprintf(file, "%c", 0x7b);
  printf("Creating Header section.\n");
  fprintf(file, "%c", 0x0d);
  if (choose_name(file) == ERROR)
    {
      fprintf(stderr, "Error on fullfil name (init_struct).\n");
      return (ERROR);
    }
  if ((nb_room = set_rooms_st_end(file)) == ERROR)
    {
      fprintf(stderr, "Error on settings rooms (init_struct).\n");
      return (ERROR);
    }
  return (nb_room);
}

int			choose_name(FILE *file)
{
  char			*name;
  int			len;

  printf("Please choose your game name (<255 caracteres).\n");
  if (!(name = get_next_line(0)))
    {
      fprintf(stderr, "Error on read (get_next_line).\n");
      return (ERROR);
    }
  fprintf(file, "%c", 0x01);
  write_str_in_file(file, name, strlen(name));
  return (SUCCESS);
}

int			set_rooms_st_end(FILE *file)
{
  int			nb_room;
  char			*s;
  int			yes;

  yes = 0;
  printf("Map configuration ...\n");
  printf("How many rooms do you want ? (2-100)\n");
  while (!yes)
    {
      if (!(s = get_next_line(0)))
	{
	  fprintf(stderr, "Error on read ! (set_rooms_st_end)\n");
	  return (ERROR);
	}
      ((nb_room = atoi(s)) <= 1 || nb_room > 100) ?
	(fprintf(stderr, "Bad number. Retry.\n")) : (yes = 1);
    }
  fprintf(file, "%c", 0x03);
  write_str_in_file(file, "ROOM1", strlen("ROOM1"));
  if ((write_end_room(file, nb_room)) == ERROR)
    {
      fprintf(stderr, "error in write_end_room (in game.c)");
      return (ERROR);
    }
  return (nb_room);
}
