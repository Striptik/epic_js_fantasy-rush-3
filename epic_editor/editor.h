/*
** editor.h for editor header in /home/loisel_k/Taff/C/C-Prog-Elem/B2/epic_js_fantasy-rush-3/epic_editor
** 
** Made by loisel_k
** Login   <loisel_k@epitech.net>
** 
** Started on  Sun May 11 17:48:12 2014 loisel_k
** Last update Sat May 10 08:40:44 2014 Augustin Saint Olive
*/

#ifndef _EDITOR_H_
# define _EDITOR_H_

int	*my_init_malloc_bot(int *bot_list);
int	init_monster_by_room(FILE *fd, int room_nbr);
void	my_fprintf_room_to_file(int *bot_list, int room, FILE *fd, int room_max);
void	my_end_printf_room(int *bot_list, FILE *fd);
void	my_add_monster_to_room(int *bot_list, int i, FILE *fd);
int	*my_final_room_init(int *bot_list);
int	my_monster_nbr(int room);
int	my_bot_select(int i);
int	bot_init(FILE *fd);
int	my_get_len_room(int bot_list, int len);
int	init_header(FILE *file);

# define ERROR		-1
# define SUCCESS	0


#endif /* !_EDITOR_H_ */
