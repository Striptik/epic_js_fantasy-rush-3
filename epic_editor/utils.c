/*
** utils.c for fonctions utiles sa Mere in /home/loisel_k/Taff/C/C-Prog-Elem/B2/epic_js_fantasy-rush-3/epic_editor
** 
** Made by loisel_k
** Login   <loisel_k@epitech.net>
** 
** Started on  Sun May 11 17:51:06 2014 loisel_k
** Last update Sun May 11 17:52:31 2014 loisel_k
*/

#include <stdio.h>
#include "editor.h"
#include "get_next_line.h"

int			write_str_in_file(FILE *file, char *name, int len)
{
  if (!name || !len || !file)
    {
      fprintf(stderr, "Impossible to write in the file.\n");
      return (ERROR);
    }
  fprintf(file, "%c", len);
  fprintf(file, "%s", name);
  return (SUCCESS);
}
