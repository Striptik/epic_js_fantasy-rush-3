/*
** write_room.c for write_room in /home/saint-_a/Downloads/epic_editor/epic_editor
** 
** Made by Augustin Saint Olive
** Login   <saint-_a@epitech.net>
** 
** Started on  Sat May 10 08:57:23 2014 Augustin Saint Olive
** Last update Sun May 11 22:28:16 2014 lohmey_d
*/

#include <stdio.h>
#include <stdlib.h>
#include "editor.h"

int	init_monster_by_room(FILE *fd, int room_nbr)
{
  int	room;
  int	bot;
  int	i;
  int	*bot_list;

  room = 1;
  if ((bot_list = my_init_malloc_bot(bot_list)) == NULL)
    return (ERROR);
  while (room <= room_nbr)
    {
      i = 0;
      if (room == room_nbr)
	{
	  bot_list = my_final_room_init(bot_list);
	  i = i + 1;
	}
      while ((bot = my_monster_nbr(room)) == -1);
      while (i < bot)
	{
	  bot_list[i] = my_bot_select(i);
	  i = i + 1;
	}
      my_fprintf_room_to_file(bot_list, room, fd, room_nbr);
      room = room + 1;
    }
}

int	*my_init_malloc_bot(int *bot_list)
{
  int	i;

  i = 0;
  if ((bot_list = malloc(sizeof(int) * 12)) == NULL)
    return (NULL);
  while (i < 12)
    {
      bot_list[i] = 0;
      i = i + 1;
    }
  return (bot_list);
}

void	my_fprintf_room_to_file(int *bot_list, int room, FILE *fd, int room_max)
{
  if (room == 1)
    fprintf(fd, "%c%c%c%s%i%c%c%s%c%c%s", 15, 1, 6, "ROOM", 
	    room, 10, 4, "NONE", 11, 6, "ROOM02");
  else if (room == room_max && room_max <= 9)
    fprintf(fd, "%c%c%c%s%i%c%c%s%c%c%s", 15, 1, 6, "ROOM0",
	    room, 10, 4, "NONE", 11, 4, "NONE");
  else if (room == room_max && room_max > 9)
    fprintf(fd, "%c%c%c%s%i%c%c%s%c%c%s", 15, 1, 6, "ROOM",
	    room, 10, 4, "NONE", 11, 4, "NONE");
  else if (room < 9)
    fprintf(fd, "%c%c%c%s%i%c%c%s%c%c%s%i", 15, 1, 6, "ROOM0",
	    room, 10, 4, "NONE", 11, 6, "ROOM0", room + 1);
  else if (room == 9)
    fprintf(fd, "%c%c%c%s%i%c%c%s%c%c%s%i", 15, 1, 6, "ROOM0",
	    room, 10, 4, "NONE", 11, 6, "ROOM", room + 1);
  else
    fprintf(fd, "%c%c%c%s%i%c%c%s%c%c%s%i", 15, 1, 6, "ROOM",
	    room, 10, 4, "NONE", 11, 6, "ROOM", room + 1);
  if (bot_list[0] == 0)
    fprintf(fd,"%c%cNONE%c", 18, 4, 10);
  else
    my_end_printf_room(bot_list, fd);
}

void	my_end_printf_room(int *bot_list, FILE *fd)
{
  int	i;
  int	len;
  char	*monster;

  len = -1;
  i = 0;
  while (bot_list[i] != 0)
    {
      len = my_get_len_room(bot_list[i], len);
      i++;
    }
  i = 0;
  fprintf(fd, "%c%c", 18, len);
  while (bot_list[i] != 0)
    {
      my_add_monster_to_room(bot_list, i, fd);
      i = i + 1;
      if (bot_list[i] != 0)
	fprintf(fd, "=");
    }
  fprintf(fd, "%c", 10);
}

int	my_get_len_room(int bot_list, int len)
{
  if (bot_list == 1)
    len = len + 5;
  if (bot_list == 2)
    len = len + 10;
  if (bot_list == 5)
    len = len + 9;
  if (bot_list == 3)
    len = len + 16;
  if (bot_list == 4)
    len = len + 10;
  return (len);
}
