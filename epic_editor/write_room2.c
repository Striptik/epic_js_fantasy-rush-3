/*
** write_room2.c for write_room2 in /home/saint-_a/Downloads/epic_editor/epic_editor
** 
** Made by Augustin Saint Olive
** Login   <saint-_a@epitech.net>
** 
** Started on  Sat May 10 09:03:16 2014 Augustin Saint Olive
** Last update Sat May 10 09:06:00 2014 Augustin Saint Olive
*/

#include <stdio.h>
#include <stdlib.h>
#include "editor.h"

void	my_add_monster_to_room(int *bot_list, int i, FILE *fd)
{
  if (bot_list[i] == 1)
    fprintf(fd, "JOLF");
  if (bot_list[i] == 2)
    fprintf(fd, "DATACONDA");
  if (bot_list[i] == 3)
    fprintf(fd, "DRAGON CHEENOIS");
  if (bot_list[i] == 4)
    fprintf(fd, "TEDDYBEAR");
  if (bot_list[i] == 5)
    fprintf(fd, "JSCARGOT");
}

int	*my_final_room_init(int *bot_list)
{
  printf("\n\n\nYou arrive in the last room of the donjon. ");
  printf("You are going to fight ");
  printf("against the terrible Jscargot, also call \"the Asian Eater\", at");
  printf(" the same time than the others than you will choose.\n\n");
  bot_list[0] = 5;
  return (bot_list);
}

int	my_monster_nbr(int room)
{
  int	bot;

  bot = 0;
  printf("\n\n\t ROOM %i\n\nHow many monsters do you want in", room); 
  printf(" room %i ?\nThe number of monsters in the room %i : ", room, room);
  scanf("%d", &bot);
  if (bot < 0 || bot > 10)
    {
      printf("ERROR : Invalide number of monster.\n\n");
      return (-1);
    }
  return (bot);
}

int	my_bot_select(int i)
{
  int	bot;

  bot = -1;
  while (bot == -1)
    {
      printf("Monsters table :\n1: Jolf : Evil kind of wolf with bloody ");
      printf("shining eyes.\n2: Dataconda : Huge brown snake with really ");
      printf("high speed.\n3: Dragon cheenois : Fire-eater drake looking ");
      printf("like a worm.\n4: Teddybear : Bear whith dark black long fur");
      printf(" and golden teeth.\nChoose the number of the monster you");
      printf(" want for monster%i : ", i + 1);
      scanf("%i", &bot);
      if (bot < 1 || bot > 4)
	{
	  printf("Invalide type.\n");
	  printf("Invalid type.\n");
	  bot == -1;
	}
    }
  return (bot);
}

int	bot_init(FILE *fd)
{
  fprintf(fd, "%c%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c",
	  14, 4, 4, "JOLF", 5, 2, "50", 20, 2, "50", 6, 2, "50", 7, 2, "20",
	  8, 4, "NONE", 9, 4, "NONE", 10);
  fprintf(fd, "%c%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c",
	  14, 4, 9, "DATACONDA", 5, 2, "30", 20, 2, "40", 6, 2, "90", 7, 2,
	  "30", 8, 4, "NONE", 9, 4, "NONE", 10);
  fprintf(fd, "%c%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c",
	  14, 4, 15, "DRAGON CHEENOIS", 5, 2, "80", 20, 2, "70", 6, 2, "30",
	  7, 2, "50", 8, 4, "NONE", 9, 4, "NONE", 10);
  fprintf(fd, "%c%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c",
	  14, 4, 9, "TEDDYBEAR", 5, 2, "60", 20, 2, "60", 6, 2, "40", 7, 2,
	  "40", 8, 4, "NONE", 9, 4, "NONE", 10);
  fprintf(fd, "%c%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c%c%s%c",
	  14, 4, 8, "JSCAROT", 5, 3, "150", 20, 2, "60", 6, 2, "10", 7, 2,
	  "10", 8, 4, "NONE", 9, 4, "NONE", 10);
}
