/*
** test.c for test in /home/loisel_k/Taff/C/C-Prog-Elem/B2/Rush3
** 
** Made by loisel_k
** Login   <loisel_k@epitech.net>
** 
** Started on  Sat May 10 03:19:07 2014 loisel_k
** Last update Sat May 10 04:47:49 2014 loisel_k
*/

#include "test_head.h"

static const t_value		g_values[] = {
  { "NAME", 0x01 },
  { "ROOM_TO_WIN", 0x02 },
  { "ROOM_TO_START", 0x03 },
  { "TYPE", 0x04 },
  { "HP", 0x05 },
  { "SPEED", 0x06 },
  { "DEG", 0x07 },
  { "WEAPON", 0x08 },
  { "ARMOR", 0x09 },
  { "ADV", 0x10 },
  { "TAB=>CONNECTION", 0x11 },
  { "TAB=>MONSTER", 0x12 },
  { "ROOM", 0x0F },
  { "SPE", 0x20 },
  { "CHAMPION", 0x0C },
  { "HEADER", 0x0D },
  { "MONSTER", 0x0E },
  { "SEP_SECTION", 0x0A },
  { "MAGIC_NUMBER", 0x123 },
};
