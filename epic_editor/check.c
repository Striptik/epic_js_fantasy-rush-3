/*
** check.c for check in /home/saint-_a/Downloads/epic_editor/epic_editor
** 
** Made by Augustin Saint Olive
** Login   <saint-_a@epitech.net>
** 
** Started on  Sat May 10 08:54:04 2014 Augustin Saint Olive
** Last update Sat May 10 08:54:42 2014 Augustin Saint Olive
*/

#include <string.h>

# define ERROR          -1
# define SUCCESS        0

int	check_weapon(char *type)
{
  if (strncmp(type, "sword", 7) == 0)
    return (0);
  if (strncmp(type, "bow", 6) == 0)
    return (0);
  if (strncmp(type, "dildo", 7) == 0)
    return (0);
  if (strncmp(type, "stick", 7) == 0)
    return (0);
  return (ERROR);
}
