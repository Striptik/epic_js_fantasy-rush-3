/*
** game.c for game in /home/saint-_a/Downloads/epic_editor/epic_editor
** 
** Made by Augustin Saint Olive
** Login   <saint-_a@epitech.net>
** 
** Started on  Sat May 10 08:54:30 2014 Augustin Saint Olive
** Last update Sat May 10 08:54:39 2014 Augustin Saint Olive
*/

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include "editor.h"
#include "get_next_line.h"

int	write_end_room(FILE *file, int nb_rooms)
{
  char	*last_room;
  char	*rooms;

  if ((last_room = malloc(sizeof(char) * 7)) == NULL ||
      (rooms = malloc(sizeof(char) * 4)) == NULL)
    return (ERROR);
  fprintf(file, "%c", 0x02);
  if (nb_rooms >= 10)
    {
      fprintf(file, "%c", 0x06);
      rooms[0] = (nb_rooms / 10) + 48;
      rooms[1] = (nb_rooms % 10) + 48;
      rooms[2] = 0;
    }
  else
    {
      fprintf(file, "%c", 0x05);
      rooms[0] = nb_rooms + 48;
      rooms[1] = 0;
    }
  if ((last_room = strdup("ROOM")) == NULL)
    return (ERROR);
  last_room = strcat(last_room, rooms);
  fprintf(file, "%s", last_room);
}

int	check_type(char *type)
{
  if (strncmp(type, "warrior", 7) == 0)
    return (0);
  if (strncmp(type, "wizard", 6) == 0)
    return (0);
  if (strncmp(type, "templar", 7) == 0)
    return (0);
  return (ERROR);
}

int	choose_type(FILE *file)
{
  char	*type;
  int	ok;

  ok = 0;
  printf("Champion initialising\n");
  while (ok == 0)
    {
      printf("Choose the type of your champ: warrior, wizard or templar\n");
      if (!(type = get_next_line(0)))
	{
	  fprintf(stderr, "Error on read (in choose_type)\n");
	  return (ERROR);
	}
      if ((check_type(type)) == ERROR)
	printf("Wrong type, retrying\n");
      else
	ok = 1;
    }
  fprintf(file, "%c", 0x04);
  printf("Your type is %s\n", type);
  write_str_in_file(file, type, strlen(type));
  return (0);
}

int	choose_weapon(FILE *file)
{
  char	*weapon;
  int	ok;

  ok = 0;
  while (ok == 0)
    {
      printf("Now choose your weapon: sword, bow, dildo, stick\n");
      if (!(weapon = get_next_line(0)))
	{
	  fprintf(stderr, "Error on read in choose_weapon (in game.c)\n");
	  return (ERROR);
	}
      if ((check_weapon(weapon)) == ERROR)
	printf("Wrong weapon, retrying\n");
      else
	ok = 1;
    }
  fprintf(file, "%c", 0x08);
  printf("Your weapon is %s\n", weapon);
  write_str_in_file(file, weapon, strlen(weapon));
}

int	init_champ(FILE *file)
{
  char	*champion;
  char	*choice;

  printf("Creating champion\nChoose a name for your champion (2-100)\n");
  fprintf(file, "%c", 0x0c);
  if (!(champion = get_next_line(0)))
    {
      fprintf(stderr, "Error on read (init_champ)\n");
      return (ERROR);
    }
  fprintf(file, "%c", 0x01);
  printf("The name of your champion is %s !\n", champion);
  write_str_in_file(file, champion, strlen(champion));
  if ((choose_type(file)) == ERROR)
    return (ERROR);
  if ((choose_weapon(file)) == ERROR)
    return (ERROR);
  printf("do you want to create another champion?\nWrite 'yes' or 'no'.");
  printf(" If you don't, answer will be 'no'\n");
  if (!(choice = get_next_line(0)))
    return (ERROR);
  if (strncmp(choice, "yes", 3) == 0)
    init_champ(file);
  else
    return (0);
}
