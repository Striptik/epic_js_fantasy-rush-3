/*
** main.c for main epic creator in /home/loisel_k/Taff/C/C-Prog-Elem/B2/Rush3/epic_editor
** 
** Made by loisel_k
** Login   <loisel_k@epitech.net>
** 
** Started on  Sat May 10 16:48:02 2014 loisel_k
** Last update Sun May 11 17:56:22 2014 loisel_k
*/

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include "editor.h"
#include "get_next_line.h"

int			init_game(FILE *file)
{
  int			nb_room;

  if ((nb_room = init_header(file)) == ERROR)
    return (ERROR);
  init_monster_by_room(file, nb_room);
  if (init_champ(file) == ERROR)
    return (ERROR);
  fprintf(file, "%c", 0x0a);
}

int			main(void)
{
  FILE			*file;

  if (!(file = fopen("./conf_game","w+")))
    {
      fprintf(stderr, "Error to open file.\n");
      return (ERROR);
    }
  if (init_game(file) == ERROR)
    return (ERROR);
  return (0);
}
