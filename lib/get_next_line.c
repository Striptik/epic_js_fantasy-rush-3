/*
** get_next_line.c for get_next_line in /home/moreau_0/epic_js_fantasy-rush-3
** 
** Made by 
** Login   <moreau_0@epitech.net>
** 
** Started on  Sat May 10 04:25:52 2014 
** Last update Sun May 11 17:49:06 2014 
*/

#include <stdlib.h>
#include <string.h>
#include "get_next_line.h"

int		my_strchar(char *str, char c)
{
  int		i;

  i = 0;
  while (str[i] != 0)
    {
      if (str[i] == c)
	return (i);
      i++;
    }
  return (-1);
}

char		*my_end_of(char *buff)
{
  int		i;
  char		*str;

  if ((i = my_strchar(buff, '\n')) == -1)
    return (NULL);
  str = my_dup(buff + i + 1);
  return (str);
}

char		*this_is_the_end(char *total, char **left, int ret)
{
  int		i;

  if ((*left = my_end_of(total)) == NULL && ret == GET_BUFF - 1)
    return (NULL);
  if ((i = my_strchar(total, '\n')) == -1)
    return (total);
  total[i] = 0;
  return (total);
}

char		*get_next_line(const int fd)
{
  char		buff[GET_BUFF];
  int		ret;
  char		*total;
  static char   *left = NULL;

  my_memset(buff, GET_BUFF);
  if (left == NULL)
    if ((left = strdup("\0")) == NULL)
      return (NULL);
  if ((total = strdup(left)) == NULL)
    return (NULL);
  if (my_strchar(total, '\n') != -1)
    return (this_is_the_end(total, &left, GET_BUFF - 1));
  while ((ret = read(fd, buff, GET_BUFF - 1)) > 0)
    {
      buff[ret] = 0;
      if ((total = strcat(total, buff)) == NULL)
	return (NULL);
      if (my_strchar(total, '\n') != -1 || ret < GET_BUFF - 1)
	return (this_is_the_end(total, &left, ret));
      my_memset(buff, GET_BUFF);
    }
  return (NULL);
}
