/*
** my_wordmy_tab_quotes.c for str_to_wordmy_tab in /home/phiste_n/rendu/JaiUnGrosZizi
** 
** Made by Nicolas Phister
** Login   <phiste_n@epitech.net>
** 
** Started on  Mon May  5 06:22:13 2014 Nicolas Phister
** Last update Wed May  7 09:55:09 2014 quentin
*/

#include <stdlib.h>
#include "my.h"

int	my_tablen(char **my_tab)
{
  int	i;

  i = 0;
  while (my_tab[i] != 0)
    i++;
  return (i);
}

int	add_new_word(char **my_tab, char **new, int i, int k)
{
  if ((new[k] = my_strcat(new[k], " ")) == NULL)
    return (1);
  if ((new[k] = my_strcat(new[k], my_tab[i])) == NULL)
    return (1);
  return (0);
}

void	found_a_quote2(char **my_tab, char **new, int *i, int *k)
{
  if (new[*k][my_strlen(new[*k]) - 1] == '"'
      || new[*k][my_strlen(new[*k]) - 1] == '\'')
    new[*k][my_strlen(new[*k]) - 1] = 0;
  if (my_tab[*i] != 0)
    *i += 1;
  *k += 1;
}

int	found_a_quote(char **my_tab, char **new, int *i, int *k)
{
  int	first;

  first = 0;
  while (END_QUOTE_WHILE)
    {
      if (first == 0)
	{
	  if ((new[*k] = my_strdup(my_tab[*i] + 1)) == NULL)
	    return (1);
	}
      else if (add_new_word(my_tab, new, *i, *k) == 1)
	return (1);
      *i += 1;
      first = 1;
    }
  if (END_QUOTE_IF)
    if (first == 0)
      {
	if ((new[*k] = my_strdup(my_tab[*i] + 1)) == NULL)
	  return (1);
      }
    else if (add_new_word(my_tab, new, *i, *k) == 1)
      return (1);
  found_a_quote2(my_tab, new, i, k);
  return (0);
}

char	**my_quotes(char **my_tab)
{
  int	i;
  int	k;
  char	**new;

  i = 0;
  k = 0;
  if ((new = malloc(sizeof(*new) * (my_tablen(my_tab) + 2))) == NULL)
    return (NULL);
  while (my_tab[i] != 0)
    if (my_tab[i][0] == '"' || my_tab[i][0] == '\'')
      {
	if (found_a_quote(my_tab, new, &i, &k) == 1)
	  return (NULL);
      }
    else
      new[k++] = my_tab[i++];
  new[k] = 0;
  return (new);
}
