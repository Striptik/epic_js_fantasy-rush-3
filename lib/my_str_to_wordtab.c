/*
** my_str_to_wordtab.c for my_str_to_wordtab in /home/moreau_0/epic_js_fantasy-rush-3
** 
** Made by 
** Login   <moreau_0@epitech.net>
** 
** Started on  Sat May 10 04:26:24 2014 
** Last update Sat May 10 04:26:26 2014 
*/

#include <stdlib.h>

int	nb_word(char *str)
{
  int	a;
  int	cpt;

  a = 0;
  cpt = 0;
  while (str[a] != '\n' && str[a] != '\0')
    {
      if (str[a] == ' ' || str[a] == '\n' || str[a] == '\t')
        {
          while ((str[a] == '\t' || str[a] == ' ') && str[a] != '\n'
		 && str[a] != '\0')
	    a++;
	  cpt++;
        }
      a++;
    }
  return (cpt);
}

int	lenght_word(char *str, int nb)
{
  int   a;

  a = 0;
  while (str[nb + a] != '\0' && str[nb + a] != ' ' && str[nb + a] != '\n')
    a = a + 1;
  return (a);
}

char	**my_str_to_wordtab(char *str)
{
  int	a;
  int	b;
  int	lo;
  char	**tab;

  a = 0;
  lo = 0;
  if ((tab = malloc(sizeof(char*) * (nb_word(str) + 2))) == NULL)
    return (NULL);
  while (lo <= nb_word(str))
    {
      b = 0;
      if ((tab[lo] = malloc(lenght_word(str, a) + 1)) == NULL)
        return (NULL);
      while (str[a] != '\0' && (str[a] == ' ' || str[a] == '\t'))
	a++;
      while (str[a] != '\t' && str[a] != ' ' &&
             str[a] != '\n' && str[a] != '\0')
        tab[lo][b++] = str[a++];
      a = a + 1;
      tab[lo][b] = 0;
      lo = lo + 1;
    }
  tab[lo] = NULL;
  return (tab);
}
