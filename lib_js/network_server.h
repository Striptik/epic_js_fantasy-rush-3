/*
** network_server.h for network_server.h in /home/js/rush_bocal/lib
**
** Made by thibau_j
** Login   <thibau_j@epitech.net>
**
** Started on  Mon May  5 21:47:05 2014 thibau_j
** Last update Sat May 10 16:57:10 2014 
*/

#ifndef NETWORK_SERVER_H_
# define NETWORK_SERVER_H_

# define FD_FREE 0
# define FD_CLIENT 1
# define FD_SERVER 2
# define MAX_FD 255
# define MAX_MOB 3
# define MAX_CO 3
# define MAX_ROOM 42
# define NAME 0x01
# define END_ROOM 0x02
# define START_ROOM 0x03
# define TYPE 0x04
# define HP 0x05
# define SPEED 0x06
# define DMG 0x07
# define WEAPON 0x08
# define ARMOR 0x09
# define ADV 0x10
# define TAB_CONNECTION 0x11
# define TAB_MONSTER 0x12
# define SEPARATOR 0x0A
# define CHAMP 0x0C
# define HEADER 0x0D
# define MONSTER 0x0E
# define ROOM 0x0F
# define MAGIC_NBR 0x7B


typedef void(*fct)();

typedef struct	s_env
{
  int		fd_type[MAX_FD];
  fct		fct_read[MAX_FD];
  fct		fct_write[MAX_FD];
  int		port;
  int		mode[MAX_FD];
  size_t	size[MAX_FD];
  char		*tab[MAX_FD];
  void		(*listen_read)(char *buff, size_t size, int id);
}		t_env;

typedef struct	s_client
{
  int		id;
  char		*name;
  char		*type;
  int		hp;
  int		spe;
  int		speed;
  int		dmg;
  char		*weapon;
  char		*armor;
}		t_client;

typedef struct	s_monster
{
  char		*type;
  int		hp;
  int		spe;
  int		speed;
  int		dmg;
  char		*weapon;
  char		*armor;
}		t_monster;

typedef struct	s_room
{
  char		*name;
  char		*adv;
  char		**room_connection[MAX_CO];
  t_monster	*mob_connection[MAX_MOB];
}		t_room;

typedef struct	s_game
{
  char		*name;
  t_client	*client_tab;
  t_room	room_tab[MAX_ROOM];
}		t_game;

/*
** Port can be set but if you put 0 it will be set at 4242.
*/

void			server_loop(int port, void (*listen_read)
				    (char *buff, size_t size, int id));

/*
** Close_server : use it when you want to quit the server.
*/

void			close_server();

#endif /* !NETWORK_SERVER_H_ */
