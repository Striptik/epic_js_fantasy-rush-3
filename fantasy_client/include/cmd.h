#ifndef CMD_H_
# define CMD_H_

typedef struct		s_cmd
{
  char			*name;
  int			(*fct)(char **cmd);
}			t_cmd;

int			cmd_next(char **cmd);
int			cmd_list_team(char **cmd);
int			cmd_list_monster(char **cmd);
int			cmd_attack(char **cmd);
int			cmd_attack_spe(char **cmd);
int			cmd_noel(char **cmd);

#endif /* !CMD_H_ */
