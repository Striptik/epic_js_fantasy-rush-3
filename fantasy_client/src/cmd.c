/*
** cmd.c for  in /home/delafo_c/rendu/epic_js_fantasy-rush-3/js_server
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Sun May 11 12:52:26 2014 delafo_c
** Last update Sun May 11 13:01:59 2014 lohmey_d
*/

#include <string.h>
#include <stdio.h>
#include "../include/cmd.h"

int		tab_len(char **tab)
{
  int		i;

  i = 0;
  while (tab[i] != NULL)
    i++;
  return (i);
}

int		cmd_next(char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 2)
    {
      fprintf(stderr, "\033[31;01mUsage: next <room_name>\n\033[0;m");
      return (-1);
    }
  sprintf(packet, "OK CMD:%s %s\n", cmd[0], cmd[1]);
  write_on_server(packet, strlen(packet));
  return (0);
}

int		cmd_list_team(char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 1)
    {
      fprintf(stderr, "\033[31;01mUsage: list_team\n\033[0;m");
      return (-1);
    }
  sprintf(packet, "OK CMD:%s\n", cmd[0]);
  write_on_server(packet, strlen(packet));
  return (0);
}

int		cmd_list_monster(char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 1)
    {
      fprintf(stderr, "\033[31;01mUsage: list_monster\n\033[0;m");
      return (-1);
    }
  sprintf(packet, "OK CMD:%s\n", cmd[0]);
  write_on_server(packet, strlen(packet));
  return (0);
}

int		cmd_noel(char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 1)
    {
      fprintf(stderr, "\033[31;01mUsage: who\n\033[0;m");
      return (-1);
    }
  sprintf(packet, "OK CMD:%s\n", cmd[0]);
  write_on_server(packet, strlen(packet));
  return (0);
}
