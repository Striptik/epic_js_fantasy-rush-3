#include <string.h>
#include <stdio.h>
#include "../include/cmd.h"

int		cmd_attack(char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 2)
    {
      fprintf(stderr, "\033[31;01mUsage: attack <monster>\n\033[0;m");
      return (-1);
    }
  sprintf(packet, "OK CMD:%s %s\n", cmd[0], cmd[1]);
  write_on_server(packet, strlen(packet));
  return (0);
}

int		cmd_attack_spe(char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 2)
    {
      fprintf(stderr, "\033[31;01mUsage: attack_spe <monster>\n\033[0;m");
      return (-1);
    }
  sprintf(packet, "OK CMD:%s %s\n", cmd[0], cmd[1]);
  write_on_server(packet, strlen(packet));
  return (0);
}

int		cmd_who(char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 1)
    {
      fprintf(stderr, "\033[31;01mUsage: who\n\033[0;m");
      return (-1);
    }
  sprintf(packet, "OK CMD:%s\n", cmd[0]);
  write_on_server(packet, strlen(packet));
  return (0);
}
