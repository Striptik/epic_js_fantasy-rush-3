/*
** help.c for help in /home/lohmey_d/rendu/epic_js_fantasy-rush-3/js_server
** 
** Made by lohmey_d
** Login   <lohmey_d@epitech.net>
** 
** Started on  Sun May 11 12:55:21 2014 lohmey_d
** Last update Sun May 11 13:17:51 2014 lohmey_d
*/

#include "../include/help.h"

static t_help	tab_help[] = {
  {"next", &help_next},
  {"list_team", &help_list_team},
  {"list_monster", &help_list_monster},
  {"attack", &help_attack},
  {"attack_spe", &help_attack_spe},
  {"noel", &help_noel},
};

int		cmd_help(char **cmd)
{
  int		i;

  i = 0;
  puts("Usage : my_command <command_argument>\n");
  puts("List of commands :");
  while (i < 6)
    {
      tab_help[i].fct(i);
      i++;
    }
  return (-1);
}

void		help_next(int index)
{
  printf("%d\tnext <room_name>\n", index + 1);
}

void		help_list_team(int index)
{
  printf("%d\tlist_team\n", index + 1);
}

void		help_list_monster(int index)
{
  printf("%d\tlist_monster\n", index + 1);
}
