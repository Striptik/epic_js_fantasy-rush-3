/*
** main.c for test in /home/js/last_rush/lib
**
** Made by thibau_j
** Login   <thibau_j@epitech.net>
**
** Started on  Sat May 10 04:14:58 2014 thibau_j
** Last update Sat May 10 14:09:08 2014 thibau_j
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "../include/cmd.h"
#include "../include/help.h"
#include "../include/network_client.h"

static t_cmd    tab_cmd[] = {
  {"next", &cmd_next},
  {"list_team", &cmd_list_team},
  {"list_monster", &cmd_list_monster},
  {"attack", &cmd_attack},
  {"attack_spe", &cmd_attack_spe},
  {"noel", &cmd_noel},
  {"help", &cmd_help},
  {NULL, NULL},
};

void		handle_err(char **packet)
{
  int		i;

  i = 0;
  while ((packet[i] != NULL) && (strncmp(packet[i], "MSG:", 4) != 0))
    i++;
  printf("%s\n", packet[i] + 4);
}

void		check_packet(char *buff)
{
  char		**packet;

  packet = my_str_to_wordtab(buff);
  if (strcmp(packet[0], "ERR") == 0)
    handle_err(packet);
  else if (strcmp(packet[0], "OK") == 0)
    handle_err(packet);
  else
    printf("Invalid packet\n");
}

int		check_cmd(char *tab)
{
  int		i;
  char		**cmd;
  char		packet[1024];

  i = 0;
  cmd = my_str_to_wordtab(tab);
  while (i < 6 && strcmp(cmd[0], tab_cmd[i].name) != 0)
    i++;
  if (strcmp(cmd[0], tab_cmd[i].name) == 0)
    {
      if (tab_cmd[i].fct(cmd) == -1)
	return (-1);
    }
  else if (i == 6)
    {
      printf("Command not found: %s\n", cmd[0]);
      return (-1);
    }
  return (0);
}

void		function_read(char *buff, size_t size)
{
  char		tab[1024];
  int		ret;

  check_packet(buff);
  write(1, "FANTASY CLIENT > ", 17);
  if ((ret = read(0, tab, 1024)) == -1)
    exit(EXIT_FAILURE);
  tab[ret] = '\0';
  if (check_cmd(tab) == -1)
    function_read(buff, size);
}

int		main(int ac, char **av)
{
  if (ac < 2)
    {
      fprintf(stderr, "./fantasy_client <nb_port>\n");
      return (-1);
    }
  printf("Type <help> to print help.\n");
  if (client_init(NULL, atoi(av[1]), function_read) == -1)
    return (-1);
  client_loop();
}
