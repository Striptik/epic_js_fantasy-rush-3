/*
** main_sercer.c for server.c in /home/js/last_rush/lib
**
** Made by thibau_j
** Login   <thibau_j@epitech.net>
**
** Started on  Sat May 10 04:27:04 2014 thibau_j
<<<<<<< HEAD
** Last update Sun May 11 15:27:36 2014 lohmey_d
=======
** Last update Sun May 11 17:39:36 2014 
>>>>>>> 413de2c4bd4da1f6f7336580ba2f4d337c62a2c9
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "network_server.h"
#include "cmd.h"

static t_cmd	tab_cmd[] = {
  {"next", &cmd_next},
  {"list_team", &cmd_list_team},
  {"list_monster", &cmd_list_monster},
  {"attack", &cmd_attack},
  {"attack_spe", &cmd_attack_spe},
  {"who", &cmd_who},
  {"NULL", NULL},
};

int		check_cmd(int id, char *tab)

  int		i;
  char		**cmd;

  i = 0;
  cmd = my_str_to_wordtab(tab);
  if (!strcmp(tab, "exit"))
    close_server();
  else if (!strcmp(tab, "-help"))
    help_cmd();
  else
    {
      while (i < 6 && strcmp(cmd[0], tab_cmd[i].name) != 0)
	i++;
      if (strcmp(cmd[0], tab_cmd[i].name) == 0)
	{
	  if (tab_cmd[i].fct(id, cmd) == -1)
	    return (-1);
	}
      else if (i == 6)
	{
	  printf("Command not found: %s\n", cmd[0]);
	  return (-1);
	}
      return (0);
    }
}

int		init_game(char *game_file)
{
  if (!(g_game = malloc(sizeof(t_game))))
    return (-1);
  main_parser(game_file);
}

void		function_read(char *buff, size_t size, int id)
{
  char		tab[1024];
  int		ret;

  //  printf("%s\n", buff);
  write(1, "server> ", 8);
  if ((ret = read(0, tab, 1024)) == -1)
    exit(EXIT_FAILURE);
  tab[ret] = '\0';
  if (check_cmd(id, tab) == -1)
    function_read(buff, size, id);
  /* write_on_client(tab, strlen(tab), id); */
  /* close_server(); */
}

int	main(int ac, char **av)
{
  if (ac == 3)
    {
      if (!strcmp(av[2], "game_file"))
	{
	  init_game(av[2]);
	  server_loop(atoi(av[1]), &function_read); //attention au atoi
	}
    }
  else
    printf("Usage: ./js_server <port> <game_file>\n");
  return (0);
}
