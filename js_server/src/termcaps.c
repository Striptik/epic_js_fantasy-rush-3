#include <termios.h>
#include <unistd.h>
//#include <term.h>
#include <stdlib.h>

int		my_out(int c)
{
  write(1, &c, 1);
}

int		clean_term(void)
{
  char		*str;

  if (tgetent(NULL, "xterm") == -1)
    return (-1);
  if ((str = tgetstr("cl", NULL)) == NULL)
    return (-1);
  tputs(str, 0, my_out);
}
