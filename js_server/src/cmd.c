/*
** cmd.c for  in /home/delafo_c/rendu/epic_js_fantasy-rush-3/js_server
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Sun May 11 12:52:26 2014 delafo_c
** Last update Sun May 11 13:01:59 2014 lohmey_d
*/

#include <string.h>
#include <stdio.h>
#include "cmd.h"

int		nb = 0;

int		tab_len(char **tab)
{
  int		i;

  i = 0;
  while (tab[i] != NULL)
    i++;
  return (i);
}

int		cmd_next(int id, char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 2)
    {
      sprintf(packet, "\033[31;01mUsage: next <room_name>\n\033[0;m");
      write_on_client(packet, strlen(packet), id);
      return (-1);
    }
  /* If the room does not exist */
  sprintf(packet, "ERR NB:%d MSG:'Room does not exist'\n", ++nb);
  write_on_client(packet, strlen(packet), id);
  return (-1);
}

int		cmd_list_team(int id, char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 1)
    {
      sprintf(packet, "\033[31;01mUsage: list_team\n\033[0;m");
      return (-1);
    }
  return (-1);
}

int		cmd_list_monster(int id, char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 1)
    {
      sprintf(packet, "\033[31;01mUsage: list_monster\n\033[0;m");
      write_on_client(packet, strlen(packet), id);
      return (-1);
    }
  return (-1);
}

int		cmd_attack(int id, char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 2)
    {
      sprintf(packet, "\033[31;01mUsage: attack <monster>\n\033[0;m");
      write_on_client(packet, strlen(packet), id);
      return (-1);
    }
  sprintf(packet, "ERR NB:%d MSG:'Test'\n", ++nb);
  write_on_client(packet, strlen(packet), id);
  return (0);
}

int		cmd_attack_spe(int id, char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 2)
    {
      sprintf(packet, "\033[31;01mUsage: attack_spe <monster>\n\033[0;m");
      write_on_client(packet, strlen(packet), id);
      return (-1);
    }
  return (0);
}

int		cmd_who(int id, char **cmd)
{
  char		packet[1024];

  if (tab_len(cmd) != 1)
    {
      sprintf(packet, "\033[31;01mUsage: who\n\033[0;m");
      write_on_client(packet, strlen(packet), id);
      return (-1);
    }
  return (-1);
}
