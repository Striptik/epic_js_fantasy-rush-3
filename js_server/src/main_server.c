/*
** main_sercer.c for server.c in /home/js/last_rush/lib
**
** Made by thibau_j
** Login   <thibau_j@epitech.net>
**
** Started on  Sat May 10 04:27:04 2014 thibau_j
** Last update Sun May 11 12:38:14 2014 lohmey_d
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "network_server.h"
#include "cmd.h"
#include "help.h"

static t_cmd	tab_cmd[] = {
  {"next", &cmd_next},
  {"list_team", &cmd_list_team},
  {"list_monster", &cmd_list_monster},
  {"attack", &cmd_attack},
  {"attack_spe", &cmd_attack_spe},
  {"who", &cmd_who},
  {"help", &cmd_help},
  {NULL, NULL},
};

int		check_cmd(int id, char *tab)
{
  int		i;
  char		**cmd;
  char		packet[1024];

  i = 0;
  cmd = my_str_to_wordtab(tab);
  if (!strcmp(cmd[0], "exit"))
    close_server();
  else
    {
      while (i < 6 && strcmp(cmd[0], tab_cmd[i].name) != 0)
	i++;
      if (strcmp(cmd[0], tab_cmd[i].name) == 0)
	{
	  if (tab_cmd[i].fct(id, cmd) == -1)
	    return (-1);
	}
      else if (i == 6)
	{
	  sprintf(packet, "Command not found: %s\n", cmd[0]);
	  write_on_client(packet, strlen(packet), id);
	  return (-1);
	}
    }
  return (0);
}

int		check_packet(char *packet)
{
  return (0);
}

void		init_game(char *game_file)
{
  /*
  ** Parser
  */
}

void		function_read(char *buff, size_t size, int id)
{
  write(1, "JS SERVER > ", 12);
  printf("%s\n", buff);
  check_packet(buff);
}

int		main(int ac, char **av)
{
  if (ac == 3)
    {
      if (!strcmp(av[2], "game_file"))
	{
	  init_game(av[2]);
	  server_loop(atoi(av[1]), &function_read);
	}
    }
  else
    fprintf(stderr, "Usage: ./js_server <port> <game_file>\n");
  return (0);
}
