/*
** help.h for help in /home/lohmey_d/rendu/epic_js_fantasy-rush-3/js_server
** 
** Made by lohmey_d
** Login   <lohmey_d@epitech.net>
** 
** Started on  Sun May 11 13:11:59 2014 lohmey_d
** Last update Sun May 11 13:14:37 2014 lohmey_d
*/

#include	<stdio.h>

#ifndef		HELP_H_
# define	HELP_H_

typedef struct	s_help
{
  char		*name;
  void		(*fct)(int index);
}		t_help;

int		cmd_help(int id, char **cmd);
void            help_next(int index);
void            help_list_team(int index);
void            help_list_monster(int index);
void            help_attack(int index);
void            help_attack_spe(int index);
void            help_who(int index);

#endif		/* !HELP_H_ */
