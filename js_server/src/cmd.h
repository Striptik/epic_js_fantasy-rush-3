#ifndef CMD_H_
# define CMD_H_

typedef struct		s_cmd
{
  char			*name;
  int			(*fct)(int id, char **cmd);
}			t_cmd;

int			cmd_next(int id, char **cmd);
int			cmd_list_team(int id, char **cmd);
int			cmd_list_monster(int id, char **cmd);
int			cmd_attack(int id, char **cmd);
int			cmd_attack_spe(int id, char **cmd);
int			cmd_who(int id, char **cmd);

#endif /* !CMD_H_ */
