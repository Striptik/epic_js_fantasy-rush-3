/*
** network_server.h for network_server.h in /home/js/rush_bocal/lib
**
** Made by thibau_j
** Login   <thibau_j@epitech.net>
**
** Started on  Mon May  5 21:47:05 2014 thibau_j
<<<<<<< HEAD
** Last update Sun May 11 17:56:54 2014 
=======
** Last update Sat May 10 16:07:29 2014 lohmey_d
>>>>>>> 6cee4f017865149e779e07e889b0ace983b4cc20
*/

#ifndef NETWORK_SERVER_H_
# define NETWORK_SERVER_H_

# define FD_FREE 0
# define FD_CLIENT 1
# define FD_SERVER 2
# define MAX_FD 255
# define MAX_MOB 3
# define MAX_ROOM 42
# define MAX_CHAMP 42

#include <stdio.h>

typedef void(*fct)();

typedef struct  s_env
{
  int           fd_type[MAX_FD];
  fct           fct_read[MAX_FD];
  fct           fct_write[MAX_FD];
  int           port;
  int           mode[MAX_FD];
  size_t        size[MAX_FD];
  char          *tab[MAX_FD];
  void          (*listen_read)(char *buff, size_t size, int id);
}               t_env;

typedef struct	s_champ
{
  char		*name;
  char		*type;
  int		hp;
  int		spe;
  int		speed;
  int		dmg;
  char		*weapon;
  char		*armor;
}		t_champ;

typedef struct	s_client
{
  int		id;
  t_champ	*team[MAX_CHAMP];
}		t_client;

typedef struct	s_monster
{
  char		*type;
  int		hp;
  int		spe;
  int		speed;
  int		dmg;
  char		*weapon;
  char		*armor;
}		t_monster;

typedef struct	s_room
{
  char		*name;
  char		*adv;
  char		*tab_connection;
  t_monster	*mob_tab[MAX_MOB];
}		t_room;

typedef struct	s_game
{
  char		*name;
  t_client	client_tab[MAX_FD];
  t_room	room_tab[MAX_ROOM];
  t_champ	champ_tab[MAX_CHAMP];
  t_monster	*monster_tab[MAX_MOB];
  char		*first_room;
  char		*last_room;
}		t_game;

t_game          *g_game;

/*
** Port can be set but if you put 0 it will be set at 4242.
*/

void			server_loop(int port, void (*listen_read)
				    (char *buff, size_t size, int id));

/*
** Close_server : use it when you want to quit the server.
*/

void			close_server();

#endif /* !NETWORK_SERVER_H_ */
