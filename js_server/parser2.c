/*
** parser2.c for epic_js_fantasy in /home/moreau_0/epic_js_fantasy-rush-3/js_server
** 
** Made by 
** Login   <moreau_0@epitech.net>
** 
** Started on  Sun May 11 12:20:20 2014 
** Last update Sun May 11 17:16:31 2014 
*/

#include "network_server.h"

int		add_champ_to_team(t_champ *new)
{
  int		i;

  i = 0;
  while ((i < MAX_CHAMP) && (g_game->champ_tab[i] != NULL))
    ++i;
  if (i >= MAX_CHAMP)
    {
      printf("Maximum number of champions reached.\n");
      return (0);
    }
  else
    g_game->champ_tab[i] = new;
  return (1);
}

int		parse_champ(char *str)
{
  int		i;
  t_champ	*new;

  while (str[i])
    {
      if (str[i] == 0x01)
	new->name = get-attribute(str, &1);
      else if (str[i] == 0x04)
	new->type=get_attribute(str, &i);
      else if (str[i] == 0x05)
	new->hp=atoi(get_attribute(str, &i));
      else if (str[i] == 0x20)
	new->spe=atoi(get_attribute(str, &i));
      else if (str[i] == 0x06)
	new->speed=atoi(get_attribute(str, &i));
      else if (str[i] == 0x07)
	new->dmg=atoi(get_attribute(str, &i));
      else if (str[i] == 0x08)
	new->weapon=get_attribute(str, &i);
      else if (str[i] == 0x09)
	new->armor=get_attribute(str, &i);
      i+=2;
    }
  add_champ_to_team(new);
  return (1);
}

int		add_mob(t_monster *new)
{
  int		i;

  i = 0;
  while (i < MAX_MOB && g_game->mob_tab[i])
    i++;
  if (i >= MAX_MOB)
    {
      printf("Maximum number of monsters reached.\n");
      return (0);
    }
  else
    g_game->monster_tab[i] = new;
  return (1);
}

int		parse_mob(char *str)
{
  int		i;
  t_monster	*new;

  i = 1;
  while (str[i])
    {
      if (str[i] == 0x04)
	new->type=get_attribute(str, &i);
      else if (str[i] == 0x05)
	new->hp=atoi(get_attribute(str, &i));
      else if (str[i] == 0x20)
	new->spe=atoi(get_attribute(str, &i));
      else if (str[i] == 0x06)
	new->speed=atoi(get_attribute(str, &i));
      else if (str[i] == 0x07)
	new->dmg=atoi(get_attribute(str, &i));
      else if (str[i] == 0x08)
	new->weapon=get_attribute(str, &i);
      else if (str[i] == 0x09)
	new->armor=get_attribute(str, &i);
      i+=2;
    }
  add_mob(new);
  return (1);
}
