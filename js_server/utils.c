/*
** utils.c for utils in /home/lohmey_d/rendu/epic_js_fantasy-rush-3/js_server
** 
** Made by lohmey_d
** Login   <lohmey_d@epitech.net>
** 
** Started on  Sun May 11 15:57:39 2014 lohmey_d
** Last update Sun May 11 17:41:35 2014 lohmey_d
*/

#include	"cmd.h"
#include	"network_server.h"

int		search_room(t_game *game, char *room_name)
{
  int		i;

  i = 0;
  while (game->room_tab[i])
    {
      if (!strcmp(game->room_tab[i]->name, room_name))
	return (i);
      i++;
    }
  return (-1);
}

int		check_monster_room(int index_room, t_game *game, char *room_name)
{
  int		i;

  i = 0;
  while (game->room_tab[index]->mob_tab[i])
    i++;
  return (i);
}

int		check_next_room(
