/*
** test.h for test in /home/moreau_0/epic_js_fantasy-rush-3/js_server
** 
** Made by 
** Login   <moreau_0@epitech.net>
** 
** Started on  Sat May 10 18:17:04 2014 
** Last update Sun May 11 10:08:04 2014 lohmey_d
*/

typedef struct	s_test
{
  char		*key;
  int		value;
}		t_test;
