/*
** cmd.c for  in /home/delafo_c/rendu/epic_js_fantasy-rush-3/js_server
** 
** Made by delafo_c
** Login   <delafo_c@epitech.net>
** 
** Started on  Sun May 11 12:52:26 2014 delafo_c
** Last update Sun May 11 22:29:12 2014 lohmey_d
*/

#include <string.h>
#include <stdio.h>
#include "cmd.h"
#include "network_server.h"

int		cmd_next(int id, char **cmd, t_game *game, t_list *champ)
{
  int		nb_room;
  int		nb_monster;
  char		packet[1024];

  if (tab_len(cmd) != 2)
    {
      fprintf(stderr, "\033[31;01mUsage: next <room_name>\n\033[0;m");
      return (-1);
    }
  nb_room = search_room(game, cmd[1]);
  if (nb_room == -1)
    {
      sprintf(packet, "ERR NB:1 MSG:'%s does not exist'\n", cmd[1]);
      write_on_client(packet, strlen(packet), id);
    }
  nb_monster = check_monster_room(game, cmd[1]);
  if (nb_monster > 0)
    {
      sprintf(packet,
	      "ERR NB:1 MSG:'There is still some monsters on the room'\n", cmd[1]);
      write_on_client(packet, strlen(packet), id);
    }
  else if (nb_monster == 0)
    {
      write_on_client(packet, strlen(packet), id);
    }
  return (0);
}

int		cmd_list_team(int id, char **cmd, t_game *game, t_list *champ)
{
  int		index_team;
  char		packet[1024];

  if (tab_len(cmd) != 1)
    {
      fprintf(stderr, "\033[31;01mUsage: list_team\n\033[0;m");
      return (-1);
    }
  sprintf(packet,
	  "OK NB:2\nCHAMP:NAME=%s,TYPE=%s,HP=%d,SPEED=%d,DMG=%d,SPE=%d,WEAPON=%s,ARMOR=%s\n"
	  , game->champ_tab[index_team]->name, game->champ_tab[index_team]->type,
	  game->champ_tab[index_team]->hp, game->champ_tab[index_team]->speed,
	  game->champ_tab[index_team]->dmg, game->champ_tab[index_team]->spe,
	  game->champ_tab[index_team]->weapon, game->champ_tab[index_team]->armor);
  write_on_client(packet, strlen(packet), id);
  return (0);
}

int		cmd_list_monster(int id, char **cmd, t_game *game, t_list *champ)
{
  int		nb_of_monster;
  int		index_monster; 
  char		*room;
  char		packet[1024];

  if (tab_len(cmd) != 1)
    {
      fprintf(stderr, "\033[31;01mUsage: list_monster\n\033[0;m");
      return (-1);
    }
  nb_of_monster = get_nb_monster(game, room);
  if (nb == 0)
    {
      sprintf(packet, "ERR NB:3 MSG:\"No monsters here\"\n");
      write_on_client(packet, strlen(packet), id);
    }
  else
    {
      sprintf(packet,
	      "ERR NB:3 MONSTER:TYPE=%s,HP=%d,SPEED=%d,SPE=%d,DMG=%d,WEAPON=%s,ARMOR=%s\n"
	      , game->monster_tab[index_monster]->type, game->monster_tab[index_monster]->hp,
	      game->monster_tab[index_monster]->speed, game->monster_tab[index_monster]->spe,
	      game->monster_tab[index_monster]->dmg, game->monster_tab[index_monster]->weapon,
	      game->monster_tab[index_monster]->armor);
      write_on_client(packet, strlen(packet), id);
    }
}

int		cmd_attack(int id, char **cmd, t_game *game, t_list *champ)
{
  int		index_monster;
  char		packet[1024];

  if (tab_len(cmd) != 2)
    {
      fprintf(stderr, "\033[31;01mUsage: attack <monster>\n\033[0;m");
      return (-1);
    }
  index_monster = get_index_monster(...);
  if (index_monster == -1)
    {
      printf(packet, "ERR NB:4 MSG:\"%d doesn not exist here\"\n", cmd[1]);
      write_on_client(packet, strlen(packet), id);
    }
  else if (index_monster != -1)
    {

    }
  return (0);
}

int		cmd_attack_spe(int id, char **cmd, t_game *game, t_list *champ)
{
  int		index_monster;
  char		packet[1024];

  if (tab_len(cmd) != 2)
    {
      fprintf(stderr, "\033[31;01mUsage: attack_spe <monster>\n\033[0;m");
      return (-1);
    }
  return (0);
}

int		cmd_who(int id, char **cmd, t_game *game, t_list *champ)
{
  char		packet[1024];

  if (tab_len(cmd) != 1)
    {
      fprintf(stderr, "\033[31;01mUsage: who\n\033[0;m");
      return (-1);
    }
  return (0);
}
