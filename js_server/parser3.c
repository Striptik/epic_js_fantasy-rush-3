/*
** parser3.c for epic_js_fantasy in /home/moreau_0/epic_js_fantasy-rush-3/js_server
** 
** Made by 
** Login   <moreau_0@epitech.net>
** 
** Started on  Sun May 11 13:24:45 2014 
** Last update Sun May 11 15:29:15 2014 
*/

#include "network_server.h"

int		add_room(t_room *new)
{
  int		i;

  i = 0;
  while (i <= MAX_ROOM && g_game->room_tab[i])
    ++i;
  if (i >= MAX_ROOM)
    {
      printf("Max room number reached.\n");
      return (0);
    }
  g_game->room_tab[i] = new;
  return (1);
}

int		parse_room(char *str)
{
  int		i;
  t_room	*new;

  i = 1;
  while (str[i])
    {
      if (str[i] == 0x01)
	new->name = get_attribute(str, &i);
      else if (str[i] == 0x05)
	new->ad v= get_attribute(str, &i);
      else if (str[i] == 0x11)
	new->tab_connection = get_attribute(str, &i);
      else if (str[i] == 0x12)
	new->monster_tab = get_attribute(str, &i);
      i+=2;
    }
  add_room(new);
  return (1);
}
