/*
** parser.c for epic_js_fantasy in /home/moreau_0/epic_js_fantasy-rush-3/js_server
** 
** Made by 
** Login   <moreau_0@epitech.net>
** 
** Started on  Sat May 10 18:59:59 2014 
** Last update Sun May 11 17:12:21 2014 
*/

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "network_server.h"
#include "../lib/get_next_line.h"

char	*get_attribute(char *str, int *i)
{
  int	len;
  char	*ret;

  *i = *i + 1;
  len = (int)str[*i];
  if (!(ret = malloc(sizeof(char) * (len + 1))))
    return (NULL);
  ret[len--]  = 0;
  if (!str[*i + len])
    {
      printf("Error, size of argument isn't correct.\n");
      exit(1);
    }
  while (len >= 0)
    {
      ret[len] = str[(*i) + len];
      --len;
    }
  *i = *i + len;
  return (ret);
}

int	parse_header(char *str)
{
  int	i;

  i = 1;
  if (str[i++] != 0x0B)
    return (-1);
  while (str[i])
    {
      if (str[i] == 0x01)
	g_game->name = get_attribute(str, &i);
      else if (str[i] == 0x03)
	g_game->first_room = get_attribute(str, &i);
      else if (str[i] == 0x02)
	g_game->last_room = get_attribute(str, &i);
      i += 2;
    }
}

int	parser(char *str)
{
  if (str[0] == 0x7B)
    parse_header(str);
  else if (str[0] == 0x0C)
    parse_champ(str);
  else if (str[0] == 0x0E)
    parse_mob(str);
  else if (str[0] == 0x0F)
    parse_room(str);
}

int	main_parser(char *file)
{
  int	fd;
  char	*line;

  if ((fd = open(file, O_RDONLY)) <= 0)
    {
      printf("Error during opening file\n");
      return (-1);
    }
  while (line = get_next_line(fd))
    parser(line);
}
