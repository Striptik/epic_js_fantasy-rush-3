/*
** init.c for epic_js_fantasy in /home/moreau_0/epic_js_fantasy-rush-3/js_server
** 
** Made by 
** Login   <moreau_0@epitech.net>
** 
** Started on  Sat May 10 16:59:53 2014 
** Last update Sat May 10 20:07:06 2014 
*/

#include "network_server.h"

t_monster	*make_mob_tab(char *str_mob_tab)
{
  int		i;

  i = 0;
  
}

int		add_room(char *name, char *adv, 
			 char *next_room, char *str_mob_tab)
{
  int		i;

  i = 0;
  while (i < MAX_ROOM && g_game->room_tab[i] == NULL)
    i++;
  if (!(g_game->room_tab[i] = malloc(sizeof(t_room))))
    return (-1);
  g_game->room_tab[i]->name = name;
  g_game->room_tab[i]->adv = adv;
  g_game->room_tab[i]->next_room = next_room;
  g_game->room_tab[i]->mob_tab = make_mob_tab(str_mob_tab);
}

int		init_game(char *name)
{
  if (!(g_game = malloc(sizeof(*game))))
    return (-1);
  game->name = name;
  return (1);
}
